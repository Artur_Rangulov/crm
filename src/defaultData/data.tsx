import React from 'react';
import { Link } from '../models/models';
import {
  BarChart,
  Assignment,
  Dashboard,
  AddRounded,
  Category,
} from "@material-ui/icons";

const fontsize = 30

export const links: Link[] = [
  {
    label: 'Дашборд',
    link: '/',
    id: 'dashboard',
    icon: <Dashboard style={{ fontSize: fontsize }}/>,
  },{
    label: 'Аналитика',
    link: '/analytics',
    id: 'analytics',
    icon: <BarChart style={{ fontSize: fontsize }}/>,
  },{
    label: 'История',
    link: '/history',
    id: 'history',
    icon: <Assignment style={{ fontSize: fontsize }}/>,
  }, {
    label: 'Добавить заказ',
    link: '/addorder',
    id: 'addorder',
    icon: <AddRounded style={{ fontSize: fontsize }} />,
  }, {
    label: 'Категории',
    link: '/ranges',
    id: 'ranges',
    icon: <Category style={{ fontSize: fontsize }} />,
  }
]