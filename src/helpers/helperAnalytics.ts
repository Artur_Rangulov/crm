// import * as _ from "lodash";
import moment from "moment";
import { OrderType } from "../redux/addorder/store";

export const filterOrdersByTime = (orders: OrderType[], typeOfTime: string) => {
  const filterOrders: any = {};
  orders.forEach((order) => {
    let date = moment(order.created).format(typeOfTime);
    if (filterOrders[date]) {
      filterOrders[date].totalCost = filterOrders[date].totalCost + order.totalCost;
      filterOrders[date].count += 1;
    } else {
      filterOrders[date] = {
        count: 1,
        totalCost: order.totalCost,
      };
    }
  })
  const categories: any = [];
  const seriesDataRevenue: any = [];
  const seriesDataProducts: any = [];
  for (let item in filterOrders) {
    categories.push(item);
    seriesDataRevenue.push(filterOrders[item].totalCost);
    seriesDataProducts.push(filterOrders[item].count);
  }

  return { categories, seriesDataRevenue, seriesDataProducts };
}

export const filterOrdersByPaymentMethods = (orders: OrderType[]) => {
  const filterOrders: any = {};

  orders.forEach((order) => {
    let date = moment(order.created).format("DD.MM.YYYY");
    let paymentMethod = order.paymentMethod;
    if (!filterOrders[date]) {filterOrders[date] = {}}
    if (filterOrders[date] && filterOrders[date][paymentMethod]) {
      filterOrders[date][paymentMethod] += 1;
    } else {
      filterOrders[date][paymentMethod] = 1;
    }
  })
  const categories: any = [];
  const seriesDataCash: any = [];
  const seriesDataCreditCard: any = [];
  for (let item in filterOrders) {
    categories.push(item);
    seriesDataCash.push(filterOrders[item].cash || 0)
    seriesDataCreditCard.push(filterOrders[item].creditCard || 0)
  }


  return { categories, seriesDataCash, seriesDataCreditCard};
}

export const sumMassiveValues = (array: number[]) => {
  return array.reduce((sum: number, current: number) => {
    return sum + current
  })
}

