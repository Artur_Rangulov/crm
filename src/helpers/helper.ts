import { Action, AnyAction } from "redux";
import { GoodsType } from "../redux/ranges/store";
import moment from "moment";
// tslint:disable-next-line:naming-convention
type Reducer<S = any, A extends Action = AnyAction> = (state: S, action: A) => S;
export default function createReducer<T>(initialState: T, handlers: { [type: string]: Reducer<T> }) {
  const reducer: Reducer<T | undefined> = (state = initialState, action) => {
    if (handlers.hasOwnProperty(action.type)) {
      return handlers[action.type](state, action);
    } else {
      return state;
    }
  };
  return reducer;
}

export const validateText = (text: string): boolean => {
  return text !== ''
}

export const sliceDescription = (text: string):string => (
  text.slice(0, 150) + '...'
)

export function desc<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

export function stableSort<T>(array: T[], cmp: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

export type Order = 'asc' | 'desc';

export function getSorting<K extends keyof any>(
  order: Order,
  orderBy: K,
): (a: { [key in K]: number | string }, b: { [key in K]: number | string }) => number {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}


export const mapFirebaseData = (data: any) => {
  let mapData = [];
  for (let key in data){
    mapData.push({
      ...data[key]
    })
  }
  return mapData
}

export const filteredGoodsByRange = (goods?: GoodsType[], rangeTitle?: string) => {
  return goods && goods
    .filter((good: GoodsType) => good.range === rangeTitle)
    .map((good: GoodsType) => {
      return {
        title: good.title,
        weight: good.weight,
        cost: good.cost,
        objectId: good.objectId,
        img: good.img,
        range: good.range,
      }
    })
}

export const countGoodsOfRange = (goods: GoodsType[], rangeTitle: string): number => {
  return goods.filter(good => good.range === rangeTitle).length
}

export const parseHoursFromDate = (date: string): string => {
  return `${moment(date).format('HH')}`
}