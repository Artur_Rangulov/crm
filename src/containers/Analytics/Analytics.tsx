import React from 'react';
import { withStyles, WithStyles, createStyles} from '@material-ui/core';
import PageTitle from '../../components/atoms/PageTitle/PageTitle';
import { loadOrdersData } from '../../redux/history/actions';
import { connect } from 'react-redux';
import { IStore } from '../../redux/store';
import { OrderType } from '../../redux/addorder/store';
import ReportDailyRevenue from './Reports/ReportDailyRevenue';
import ItemsFlexComponent from '../../components/atoms/ItemsFlexComponent/ItemsFlexComponent';
import ReportTimesOfDayRevenue from './Reports/ReportTimesOfDayRevenue';
import ReportHourlyRevenue from './Reports/ReportHourlyRevenue';
import ReportPaymentMethods from './Reports/ReportPaymentMethods';

export const styles = (theme: any) => createStyles({
  wrapper:{
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
    overflow: "hidden",
  },
  component: {
    background: "#fff",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)",
    borderRadius: "4px",
    maxHeight: '350px',
    padding: 30,
    marginTop: 50,
    overflow: "hidden",
    ...theme.component,
  }
});

const mapStateToProps = (state: IStore) => {
  return {
    orders: state.orders.orders,
    loading: state.orders.loading,
  }
}

const dispathToProps = {
  loadOrdersData: loadOrdersData,
}

interface iProps {
  orders: OrderType[];
};

type Props = iProps & WithStyles<typeof styles> & typeof dispathToProps;

class Analytics extends React.PureComponent<Props> {
  componentDidMount() {
    if (this.props.orders.length === 0) {
      this.props.loadOrdersData();
    }
  }
  render(){
    const { classes } = this.props;

    return (
      <>
        <PageTitle title='Analytics' />
        <div className={classes.wrapper}>
          <ItemsFlexComponent className={classes.component} flexbasis="69%">
            <ReportDailyRevenue />            
          </ItemsFlexComponent>
          <ItemsFlexComponent className={classes.component} flexbasis="29%">
            <ReportTimesOfDayRevenue />
          </ItemsFlexComponent>
          <ItemsFlexComponent className={classes.component} flexbasis="100%">
            <ReportHourlyRevenue />
          </ItemsFlexComponent>
          <ItemsFlexComponent className={classes.component} flexbasis="100%">
            <ReportPaymentMethods />
          </ItemsFlexComponent>
        </div>

      </>
    )
  }
};

export default connect(mapStateToProps, dispathToProps)(withStyles(styles)(Analytics));
