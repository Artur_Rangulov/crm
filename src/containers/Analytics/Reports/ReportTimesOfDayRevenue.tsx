import React, { useState } from "react";
import Chart from "react-apexcharts";
import { IStore } from "../../../redux/store";
import { connect } from "react-redux";
import { OrderType } from "../../../redux/addorder/store";
import { filterOrdersByTime } from "../../../helpers/helperAnalytics";
import { Typography } from "@material-ui/core";

enum TimesOfDay{
  Morning = 0,
  Afternoon = 1,
  Evening = 2,
  Night = 3,
}

interface Props {
  orders: OrderType[];
}
const mapStateToProps = (state: IStore) => {
  return {
    orders: state.orders.orders,
  }
}

const ReportTimesOfDayRevenue: React.FunctionComponent<Props> = ({ orders }) => {
  const filterOrders = filterOrdersByTime(orders, "HH");

  console.log(TimesOfDay)

  return (
    <>
      <Typography variant='h6' component='h2' >Заказы по времени суток</Typography>
      <Chart
        width="100%"
        height="100%"
        type="pie"
        options={{
          labels: [TimesOfDay[0], TimesOfDay[1], TimesOfDay[2], TimesOfDay[3]],
          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                width: "100%"
              },
              legend: {
                position: 'bottom',
              }
            }
          }]
        }}
        series = {[44, 155, 13, 43]}
  />

    </>
  );
}

export default connect(mapStateToProps)(ReportTimesOfDayRevenue);
