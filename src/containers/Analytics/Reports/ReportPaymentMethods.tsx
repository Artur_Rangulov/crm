import React, { useState } from 'react';
import Chart from "react-apexcharts";
import { IStore } from '../../../redux/store';
import { filterOrdersByPaymentMethods, sumMassiveValues } from '../../../helpers/helperAnalytics';
import { OrderType } from '../../../redux/addorder/store';
import { connect } from 'react-redux';
import { Button, Typography } from '@material-ui/core';

interface Props {
  orders: OrderType[];
}

const mapStateToProps = (state: IStore) => {
  return {
    orders: state.orders.orders,
    loading: state.orders.loading,
  }
}

const ReportPaymentMethods: React.FunctionComponent<Props> = ({ orders }) => {
  const filterOrders = filterOrdersByPaymentMethods(orders);
  const [viewValues, setViewValues] = useState('byDays');
  const [buttonTitle, setButtonTitle] = useState('Показать всего');

  let categories, distributed, series;

  switch (viewValues) {
    case "byDays":
      categories = filterOrders.categories;
      series = [{
        name: "Наличные",
        data: filterOrders.seriesDataCash,
      }, {
        name: "Безналичные",
        data: filterOrders.seriesDataCreditCard,
      }];
      distributed = false;
      break;
    case "All":
      categories = ["Наличные", "Безналичные"];
      series = [{
        data: [
          sumMassiveValues(filterOrders.seriesDataCash),
          sumMassiveValues(filterOrders.seriesDataCreditCard),
        ]
      }];
      distributed = true;
  }

  const changeViewType = () => {
    if (viewValues === "byDays") {
      setViewValues("All");
      setButtonTitle("Показать по дням");
    }
    else {
      setViewValues("byDays")
      setButtonTitle("Показать всего");
    }
  }

  return (
    <>
      <Typography variant='h6' component='h2' >Способы оплаты</Typography>
      <Button
        variant='outlined'
        onClick={changeViewType}>{buttonTitle}
      </Button>
      <Chart
        width="100%"
        height="85%"
        type="bar"
        options={{
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: '55%',
              distributed: distributed,
            },
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
          },
          xaxis: {
            categories: categories,
          },
          yaxis: {
            title: {
              text: 'Количество'
            },
          },
        }}
        series={series}
      />
    </>
  )
}

export default connect(mapStateToProps)(ReportPaymentMethods);
