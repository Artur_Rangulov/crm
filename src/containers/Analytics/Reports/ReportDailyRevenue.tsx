import React, { useState } from "react";
import Chart from "react-apexcharts";
import { IStore } from "../../../redux/store";
import { connect } from "react-redux";
import { OrderType } from "../../../redux/addorder/store";
import { filterOrdersByTime } from "../../../helpers/helperAnalytics";
import { Button, Typography } from "@material-ui/core";

interface Props {
  orders: OrderType[];
}
const mapStateToProps = (state: IStore) => {
  return {
    orders: state.orders.orders,
  }
}

const ReportDailyRevenue: React.FunctionComponent<Props> = ({ orders }) => {
  const filterOrders = filterOrdersByTime(orders, "DD.MM.YYYY");
  const [viewValues, setViewValues] = useState('byRevenue');
  const [buttonTitle, setButtonTitle] = useState('Показать кол-во товаров');
  const [yaxiesTitle, setYaxiesTitle] = useState('Стоимость (руб.)');

  let categories, seriesData;
  switch (viewValues) {
    case "byRevenue":
      seriesData = [{
        name: "Чек",
        data: filterOrders.seriesDataRevenue,
      }];
      break;
    case "byProducts":
      seriesData = [{
        name: "Количество",
        data: filterOrders.seriesDataProducts,
      }];
      break;
  }

  const changeViewType = () => {
    if (viewValues === "byRevenue") {
      setViewValues("byProducts");
      setYaxiesTitle("Количество");
      setButtonTitle("Показать выручку");
    }
    else {
      setViewValues("byRevenue");
      setYaxiesTitle("Стоимость (руб.)");
      setButtonTitle("Показать кол-во товаров");
    }
  }

  return (
    <>
      <Typography variant='h6' component='h2' >Заказы по дням</Typography>
      <Button
        variant='outlined'
        onClick={changeViewType}>{buttonTitle}
      </Button>
      <Chart
        width="100%"
        height="85%"
        type="line"
        options={{
          chart: {
            shadow: {
              enabled: true,
              color: '#000',
              top: 18,
              left: 7,
              blur: 10,
              opacity: 1
            },
            toolbar: {
              show: false
            }
          },
          dataLabels: {
            enabled: true
          },
          stroke: {
            curve: 'straight'
          },
          grid: {
            row: {
              colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
              opacity: 0.5
            },
          },
          xaxis: {
            categories: filterOrders.categories,

          },
          yaxis: {
            title: {
              text: yaxiesTitle
            },
            min: 0,
          },
        }}
        series= {seriesData}
    />

    </>
  );
}

export default connect(mapStateToProps)(ReportDailyRevenue);
