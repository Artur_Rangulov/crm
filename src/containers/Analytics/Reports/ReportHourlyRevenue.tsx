import React, { useState } from 'react';
import Chart from "react-apexcharts";
import { IStore } from '../../../redux/store';
import { filterOrdersByTime } from '../../../helpers/helperAnalytics';
import { OrderType } from '../../../redux/addorder/store';
import { connect } from 'react-redux';
import { Button, Typography } from '@material-ui/core';

interface Props {
  orders: OrderType[];
}

const mapStateToProps = (state: IStore) => {
  return {
    orders: state.orders.orders,
    loading: state.orders.loading,
  }
}

const ReportHourlyRevenue: React.FunctionComponent<Props> = ({ orders }) => {
  const filterOrders = filterOrdersByTime(orders, "HH");

  const [viewValues, setViewValues] = useState('byRevenue');
  const [buttonTitle, setButtonTitle] = useState('Показать кол-во товаров');
  const [yaxiesTitle, setYaxiesTitle] = useState('Стоимость (руб.)');

  let seriesData;
  switch (viewValues) {
    case "byRevenue":
      seriesData = [{
        name: "Чек",
        data: filterOrders.seriesDataRevenue,
      }]
      break;
    case "byProducts":
      seriesData = [{
        name: "Количество",
        data: filterOrders.seriesDataProducts,
      }]
      break;
  }

  const changeViewType = () => {
    if (viewValues === "byRevenue") {
      setViewValues("byProducts");
      setYaxiesTitle("Количество")
      setButtonTitle("Показать выручку");
    }
    else {
      setViewValues("byRevenue")
      setYaxiesTitle("Стоимость (руб.)")
      setButtonTitle("Показать кол-во товаров");
    }
  }
  return (
    <>
      <Typography variant='h6' component='h2' >Заказы по часам</Typography>
      <Button
        variant='outlined'
        onClick={changeViewType}>{buttonTitle}
      </Button>
      <Chart
        width="100%"
        height="85%"
        type="bar"
        options={{
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: '55%',
            },
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
          },
          xaxis: {
            categories: filterOrders.categories,
          },
          yaxis: {
            title: {
              text: yaxiesTitle
            },
          },
        }}
        series={seriesData}
    />
    </>
  )
}

export default connect(mapStateToProps)(ReportHourlyRevenue);
