import React from 'react'
import { withStyles, WithStyles, Button, Typography, Divider, createStyles } from '@material-ui/core';
// import { InjectedIntlProps, defineMessages, FormattedMessage, injectIntl } from 'react-intl';

// const messages = defineMessages({
//   title: {
//     description: "Text of full selection control.",
//     id: "app.title",
//   },
// });

const styles = () => createStyles({
})

// type Props = WithStyles<typeof styles> & InjectedIntlProps
type Props = WithStyles<typeof styles>

const Home: React.FunctionComponent<Props> = (props) => {
  // const {intl: { formatMessage }} = props;
  return (
    <div>
      <div>
        <h1>Home</h1>
        <Button
          variant="contained"
          onClick={() => console.log('1')}
        >
          Switch theme
        </Button>
      </div>
      <Typography color="primary">Some text with Primary</Typography>
      <Typography color="secondary">Some text with Secondary</Typography>
      <Divider />
      <Button color="primary">Primary Button</Button>
      <Button color="secondary">Secondary Button</Button>
    </div>
  )
}

// export default withStyles(styles)(injectIntl(Home))
export default withStyles(styles)(Home)
