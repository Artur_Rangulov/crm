import React from 'react';
import { withStyles, WithStyles, createStyles } from '@material-ui/core';
import PageTitle from '../../components/atoms/PageTitle/PageTitle';

export const styles = (theme: any) => createStyles({
  wrapper: {
    marginTop: 20,
    display: 'flex',
    flexWrap: 'wrap',
  }
});

interface iProps {};

type Props = iProps & WithStyles<typeof styles>;

const Dashboard: React.FunctionComponent<Props> = ({ classes}) => {
  return (
    <>
      <PageTitle title='Dashboard' />
      <div className={classes.wrapper}>
        
      </div>
    </>
  );
}

export default withStyles(styles)(Dashboard);
