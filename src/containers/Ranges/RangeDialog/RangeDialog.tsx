import React, { useState, useMemo } from 'react'
import { Dialog } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import { addNewRange, saveEditRange } from '../../../redux/ranges/actions';
import { RangeType, GoodsType } from '../../../redux/ranges/store';
import { IStore } from '../../../redux/store';
import RangeDialogGoodsTable from './RangeDialogGoodsTable';
import { filteredGoodsByRange } from '../../../helpers/helper';

interface Props {
  className: string;
  goods: GoodsType[];
  openDialog: boolean;
  closeDialog: any;
  selectedRange?: RangeType;
  showNotification: any;
}

const mapStateToProps = (state: IStore) => {
  return {
    goods: state.range.goods,
  }
}

const dispathToProps = {
  addNewRange: addNewRange,
  saveEditRange: saveEditRange,
}

type iProps = Props & typeof dispathToProps

const RangeDialog: React.FunctionComponent<iProps> = (props) => {
  const [state, setstate] = useState({
    dialogTitle: "",
    isEdit: false,
    rangeTitle: "",
    rangeImg: "",
  })

  const { dialogTitle, isEdit, rangeTitle, rangeImg } = state;
  const {
    className,
    openDialog,
    closeDialog,
    addNewRange,
    saveEditRange,
    selectedRange,
    goods,
    showNotification,
  } = props;

  useMemo(() => {
    if (selectedRange && selectedRange.objectId) {
      setstate({
        isEdit: true,
        dialogTitle: "Изменить категорию",
        rangeImg: selectedRange.img || "",
        rangeTitle: selectedRange.title,
      });
    } else {
      setstate({
        isEdit: false,
        dialogTitle: "Добавить категорию",
        rangeImg: "",
        rangeTitle: "",
      });
    }
  }, [selectedRange])

  const _submitRange = () => {
    const defaultData = {
      title: state.rangeTitle,
      img: state.rangeImg,
    }
    if (isEdit){
      saveEditRange({
        ...defaultData,
        objectId: selectedRange && selectedRange.objectId,
      }, showNotification);
    } else {
      addNewRange({ ...defaultData});
    }
    _emptyAndCloseDialog();
  }

  const _emptyAndCloseDialog = () => {
    closeDialog();
    setstate({
      ...state,
      rangeTitle: "",
      rangeImg: "",
      dialogTitle: "",
    });
  }

  const _handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name;
    const value = event.target.value;
    setstate({
      ...state,
      [name]: value,
    });
  }

  const _validateState = (): boolean => {
    return state.rangeTitle === "";
  }


  const filteredGoods = filteredGoodsByRange(goods, rangeTitle)

  return (
    <Dialog
      open={openDialog}
      onClose={_emptyAndCloseDialog}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">{dialogTitle}</DialogTitle>
      <DialogContent className={className}>
        <TextField
          autoFocus
          margin="dense"
          name="rangeTitle"
          label="Название категории"
          type="text"
          onChange={_handleChange}
          value={rangeTitle}
          fullWidth
          disabled={isEdit}
        />
        <TextField
          margin="dense"
          name="rangeImg"
          label="Адрес картинки"
          type="text"
          onChange={_handleChange}
          value={rangeImg}
          fullWidth
        />
        {isEdit ? <RangeDialogGoodsTable goods={filteredGoods} rangeTitle={rangeTitle} showNotification={showNotification}/> : null}
      </DialogContent>
      <DialogActions>
        <Button onClick={_emptyAndCloseDialog} color="primary">Закрыть</Button>
        <Button onClick={_submitRange} color="primary" disabled={_validateState()}>{isEdit ? "Сохранить" : "Добавить"}</Button>
      </DialogActions>
    </Dialog>
  )
}

export default connect(mapStateToProps, dispathToProps)(RangeDialog)
