import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import MaterialTable from 'material-table';
import { IStore } from '../../../redux/store';
import { connect } from 'react-redux';
import { createStyles } from '@material-ui/core';
import { withStyles, WithStyles } from '@material-ui/styles';
import { addNewGood, deleteGood, saveEditGood } from '../../../redux/ranges/actions';

const styles = (theme: any) => createStyles({
  goodsTable: {
    marginTop: '20px',
  }
});

const mapStateToProps = (state: IStore) => {
  return {}
}

const dispathToProps = {
  addNewGood: addNewGood,
  deleteGood: deleteGood,
  saveEditGood: saveEditGood,
}

interface iProps {
  goods?: any;
  rangeTitle: string;
  showNotification: any;
}

type Props = iProps & typeof dispathToProps & WithStyles<typeof styles>

class RangeDialogGoodsTable extends Component<Props> {

  render() {
    const {
      goods,
      classes,
      rangeTitle,
      addNewGood,
      deleteGood,
      saveEditGood,
      showNotification
    } = this.props;
    return (
      <Paper className={classes.goodsTable}>
        <MaterialTable

          columns={[
            { title: 'название', field: 'title' },
            { title: 'вес', field: 'weight', type: 'numeric' },
            { title: 'цена', field: 'cost', type: 'numeric' },
            { title: 'objectId', field: 'objectId', hidden: true, },
            { title: 'range', field: 'range', hidden: true, },
          ]}
          data={goods}
          options={{
            actionsColumnIndex: -1
          }}
          title="Таблица продуктов"
          editable={{
            onRowAdd: newData =>
              new Promise((resolve, reject) => {
                addNewGood({
                  title: newData.title,
                  weight: +newData.weight,
                  cost: +newData.cost,
                  range: rangeTitle,
                });
                resolve();
              }),
            onRowUpdate: (newData, oldData) =>
              new Promise((resolve, reject) => {  
                saveEditGood({
                  title: newData.title,
                  weight: +newData.weight,
                  cost: +newData.cost,
                  range: newData.range,
                  objectId: newData.objectId,
                }, showNotification);
                resolve();
              }),
            onRowDelete: oldData =>
              new Promise((resolve, reject) => {
                deleteGood(oldData.objectId)
                resolve();
              })
          }}
        />
      </Paper>
    )
  }
}

export default connect(mapStateToProps, dispathToProps)(withStyles(styles)(RangeDialogGoodsTable))