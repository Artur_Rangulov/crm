import React, { Component } from 'react';
import { withStyles, WithStyles, createStyles } from '@material-ui/core';
import PageTitle from '../../components/atoms/PageTitle/PageTitle';
import RangesTable from './RangeTable/RangesTable';
import RangeDialog from './RangeDialog/RangeDialog';
import { loadRangeData } from '../../redux/ranges/actions';
import { connect } from 'react-redux';
import { RangeType, GoodsType } from '../../redux/ranges/store';
import { IStore } from '../../redux/store';
import NotificationSystem from 'react-notification-system';

export const styles = createStyles({
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: 30,
  },
  dialog: {
    minWidth: "600px",
    maxHeight: "50vh",
  },
});

const mapStateToProps = (state: IStore) => {
  return {
    goods: state.range.goods,
    ranges: state.range.ranges,
  }
}

const dispathToProps = {
  loadRangeData: loadRangeData,
}

interface iProps {
  goods: GoodsType[],
  ranges: RangeType[],
}

type Props = iProps & WithStyles<typeof styles> & typeof dispathToProps;

interface State {
  showAddRangeDialog: boolean,
  selectedRange?: RangeType,
}

class Range extends Component<Props, State> {

  state = {
    showAddRangeDialog: false,
    selectedRange: undefined,
  };

  componentDidMount() {
    if (this.props.goods.length === 0 || this.props.ranges.length === 0) {
      this.props.loadRangeData();
    }
  };

  openAddRange = (range: RangeType) => {
    this.setState({
      ...this.state,
      showAddRangeDialog: true,
      selectedRange: range,
    })
  };

  closeAddRange = () => {
    this.setState({
      ...this.state,
      showAddRangeDialog: false,
      selectedRange: undefined,
    })
  };

  showNotification = (success: boolean, message: string) => {
    const notification = this.refs.notificationSystem as NotificationSystem.System;

    notification.addNotification({
      message: message,
      position: "bl",
      level: success ? "success" : "error",
    })
  }

  render() {
    const { showAddRangeDialog, selectedRange } = this.state;
    return (
      <>
        <PageTitle
          title="Категории"
          showButton
          buttonTitle='Добавить категорию'
          onClick={this.openAddRange}
        />
        <RangesTable onShowEdit={this.openAddRange} />
        <RangeDialog
          openDialog={showAddRangeDialog}
          closeDialog={this.closeAddRange}
          selectedRange={selectedRange}
          className={this.props.classes.dialog}
          showNotification={this.showNotification}
        />
        <NotificationSystem ref="notificationSystem" />
      </>
    );
  };
}

export default connect(mapStateToProps, dispathToProps)(withStyles(styles)(Range));
