import React from 'react';
import { createStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { WithStyles } from '@material-ui/styles';
import { RangeType, GoodsType } from '../../../redux/ranges/store';
import { connect } from 'react-redux';
import { IStore } from '../../../redux/store';
import CircularLoader from '../../../components/CircularLoader/CircularLoader';
import EB from '../../../components/ErrorBoundary/ErrorBoundary';
import { deleteRange } from '../../../redux/ranges/actions';
import { countGoodsOfRange } from '../../../helpers/helper';


const styles = (theme: any) => createStyles({
  root: {
    width: '100%',
    marginTop: theme.spacing(5)
  },
  paper: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 450,
  },
});

const mapStateToProps = (state: IStore) => {
  return {
    ranges: state.range.ranges,
    goods: state.range.goods,
    loading: state.range.loading,
  }
}

const dispathToProps = {
  onDeleteRange: deleteRange,
}

interface IProps {
  ranges: RangeType[],
  goods: GoodsType[],
  loading: boolean,
  onShowEdit: any,
}

type Props = IProps & WithStyles<typeof styles> & typeof dispathToProps

const RangeTable: React.FunctionComponent<Props> = (props) => {

  const { classes, ranges, goods, loading, onShowEdit, onDeleteRange } = props;
  const isEmptyRanges = ranges && ranges.length === 0;

  const editClickHandler = (range: RangeType) => {
    onShowEdit(range)
  }

  const deleteClickHandler = (range: RangeType) => {
    onDeleteRange(range)
  }

  const table = !isEmptyRanges && (
    <Paper className={classes.paper}>
      <Table className={classes.table} size="medium">
        <TableHead>
          <TableRow>
            <TableCell>Название</TableCell>
            <TableCell>Количество блюд</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <EB>
            {
              ranges.map(row => (
                <TableRow key={row.objectId}>
                  <TableCell component="th" scope="row">
                    {row.title}
                  </TableCell>
                  <TableCell>{countGoodsOfRange(goods, row.title)}</TableCell>
                  <TableCell align='right'>
                    <Button color="primary" onClick={() => editClickHandler(row)}>
                      <EditIcon />
                    </Button>
                    <Button color="secondary" onClick={() => deleteClickHandler(row)}>
                      <DeleteIcon />
                    </Button>
                  </TableCell>
                </TableRow>
              ))
            }
          </EB>
        </TableBody>
      </Table>
    </Paper>)

  return (
    <div className={classes.root} >
      {loading ? <CircularLoader /> : (isEmptyRanges ? "Добавьте категорию" : table)}
    </div>
  );
}

export default connect(mapStateToProps, dispathToProps)(withStyles(styles)(RangeTable));
