import React from 'react'

const NotFount: React.FunctionComponent = () => {
  return (
    <div>
      Not found
    </div>
  )
}

export default NotFount
