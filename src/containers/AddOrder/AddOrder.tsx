import React, { Component } from 'react';
import * as _ from 'lodash';
import { withStyles, WithStyles, createStyles } from '@material-ui/core';
import PageTitle from '../../components/atoms/PageTitle/PageTitle';
import RangeCards from '../../components/RangeCards/RangeCards';
import { connect } from 'react-redux';
import { IStore } from '../../redux/store';
import { RangeType, GoodsType } from '../../redux/ranges/store';
import AddOrderDialog from './AddOrderDialog';
import { loadRangeData } from '../../redux/ranges/actions';
import ShowOrderDialog from './ShowOrderDialog';
import { addNewOrder } from '../../redux/addorder/actions';
import CircularLoader from '../../components/CircularLoader/CircularLoader';

export const styles = createStyles({

});

const mapStateToProps = (state: IStore) => {
  return {
    ranges: state.range.ranges,
    goods: state.range.goods,
    loading: state.range.loading,
  }
}

const dispathToProps = {
  loadRangeData: loadRangeData,
  addNewOrder: addNewOrder,
}

interface iProps {
  className?: string;
  ranges: RangeType[];
  goods: GoodsType[];
  loading: boolean;
};

interface State {
  showAddRangeDialog: boolean,
  showOrderDialog: boolean,
  selectedRange?: string,
  order: GoodsType[],
  paymentMethod: string,
}

type Props = iProps & WithStyles<typeof styles> & typeof dispathToProps;

class AddOrder extends Component<Props, State> {

  state = {
    showAddRangeDialog: false,
    showOrderDialog: false,
    selectedRange: undefined,
    order: [],
    paymentMethod: "",
  }

  componentDidMount() {
    if (this.props.goods.length === 0 || this.props.ranges.length === 0) {
      this.props.loadRangeData();
    }
  }

  render() {
    const { className, ranges, loading } = this.props;
    const { showAddRangeDialog, showOrderDialog, selectedRange, order } = this.state;

    return (
      <div className={className}>
        <PageTitle
          title="Добавить заказ"
          onClick={this._openShowOrderDialog}
          buttonTitle="Оплатить заказ"
          badgeCount={order.length}
          showButton
          showBadge
        />
        {loading ? <CircularLoader /> : <RangeCards ranges={ranges} openAddOrderDialog={this._openAddOrderDialog} />}
        <AddOrderDialog
          openDialog={showAddRangeDialog}
          closeDialog={this._closeAddOrderDialog}
          rangeTitle={selectedRange}
          addGoodToOrder={this._addGoodToOrder}
        />
        <ShowOrderDialog
          openDialog={showOrderDialog}
          closeDialog={this._closeShowOrderDialog}
          order={order}
          payOrder={this._payOrder}
          selectPaymentMethod={this._changePaymentMethod}
        />

      </div>
    );
  }

  private _payOrder = () => {
    const { order } = this.state;
    const amount = order.length;
    const orderList = _.join( order.map((item: GoodsType) => item.objectId), ",");
    const totalCost = _.sumBy(order, (item: GoodsType) => (item.cost));

    this.props.addNewOrder({
      amount,
      orders: orderList,
      totalCost,
      paymentMethod: this.state.paymentMethod,
    });

    this.setState({
      ...this.state,
      showOrderDialog: false,
      paymentMethod: "",
      order: [],
    })
  }

  private _changePaymentMethod = (method: string) => {
    this.setState({
      ...this.state,
      paymentMethod: method,
    })
  }

  private _addGoodToOrder = (good: GoodsType) => {
    const { order } = this.state;
    let array = [...order, good]
    this.setState({
      ...this.state,
      order: array,
    })
  }

  private _openShowOrderDialog = () => {
    this.setState({
      ...this.state,
      showOrderDialog: true,
    })
  }

  private _openAddOrderDialog = (rangeTitle: string) => {
    this.setState({
      ...this.state,
      showAddRangeDialog: true,
      selectedRange: rangeTitle,
    })
  }

  private _closeShowOrderDialog = () => {
    this.setState({
      ...this.state,
      showOrderDialog: false,
    })
  }

  private _closeAddOrderDialog = () => {
    this.setState({
      ...this.state,
      showAddRangeDialog: false,
    })
  }
}

export default connect(mapStateToProps, dispathToProps)(withStyles(styles)(AddOrder));
