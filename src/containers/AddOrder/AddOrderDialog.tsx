import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import { withStyles, WithStyles, createStyles } from '@material-ui/styles';
import { GoodsType } from '../../redux/ranges/store';
import { DialogActions, Button, DialogContent, DialogTitle } from '@material-ui/core';
import { filteredGoodsByRange } from '../../helpers/helper';
import { connect } from 'react-redux';
import { IStore } from '../../redux/store';
import ProductItem from '../../components/ProductItem/ProductItem';

interface iProps{
  openDialog: boolean,
  goods: GoodsType[];
  closeDialog: any,
  rangeTitle?: string,
  addGoodToOrder?: any,
}

const styles = createStyles({
  dialog: {
    padding: 21,
    maxHeight: '80vh',
    overflowY: 'auto',
  }
})

const mapStateToProps = (state: IStore) => {
  return {
    goods: state.range.goods,
  }
}

type Props = iProps & WithStyles<typeof styles>

const AddOrderDialog: React.FunctionComponent<Props> = ({
  openDialog,
  closeDialog,
  goods,
  rangeTitle,
  classes,
  addGoodToOrder,
}) => {

  const filteredGoods = filteredGoodsByRange(goods, rangeTitle);

  return (
    <div>
      <Dialog
        open={openDialog}
        onClose={closeDialog}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Добавить в заказ</DialogTitle>
        <DialogContent>
          {
            filteredGoods && filteredGoods.map(item => <ProductItem key={item.objectId} title={item.title} onClick={() => addGoodToOrder(item)}/>)
          }
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog} color="primary">Закрыть</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default connect(mapStateToProps)(withStyles(styles)(AddOrderDialog))
