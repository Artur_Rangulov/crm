import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import { DialogActions, Button, DialogContent, DialogTitle } from '@material-ui/core';
import { GoodsType } from '../../redux/ranges/store';
import Selects from '../../components/atoms/Selects/Selects';

interface iProps {
  openDialog: boolean,
  closeDialog: any,
  payOrder: any,
  order: GoodsType[],
  selectPaymentMethod: any;
}

const ShowOrderDialog: React.FunctionComponent<iProps> = ({
  openDialog,
  closeDialog,
  payOrder,
  order,
  selectPaymentMethod,
}) => {
  return (
    <div>
      <Dialog
        open={openDialog}
        onClose={closeDialog}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Оплатить заказ</DialogTitle>
        <DialogContent>
          {
            order && order.length > 0
              ? <>
                {order.map((item: GoodsType, index: number) => <p key={index}>{item.title}</p>)}
                <Selects
                  selectTitle="Способ оплаты"
                  items={[{ value: "", title: "Не выбрано" }, { value: "cash", title: "Наличные" }, { value: "creditCard", title: "Безналичные" }]}
                  selectValue={selectPaymentMethod}
                />
              </>
              : <p>Продукты не выбраны</p>
          }
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog} color="primary">Закрыть</Button>
          <Button onClick={payOrder} color="primary">Оплачено</Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default ShowOrderDialog
