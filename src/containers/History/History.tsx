import React from 'react';
import { withStyles, WithStyles, createStyles } from '@material-ui/core';
import PageTitle from '../../components/atoms/PageTitle/PageTitle';
import EnhancedTable from './EnhancedTable/EnhancedTable';
import { connect } from 'react-redux';
import { IStore } from '../../redux/store';
import { OrderType } from '../../redux/addorder/store';
import { loadOrdersData } from '../../redux/history/actions';
import LinearLoader from '../../components/LinearLoader/LinearLoader';

export const styles = (theme: any) => createStyles({
  component: {
    ...theme.component,
    marginTop: theme.spacing(5),
  },
});

const mapStateToProps = (state: IStore) => {
  return {
    orders: state.orders.orders,
    loading: state.orders.loading,
  }
}

const dispathToProps = {
  loadOrdersData: loadOrdersData,
}

interface iProps {
  orders: OrderType[],
  loading: boolean,
};

type Props = iProps & WithStyles<typeof styles> & typeof dispathToProps;

class History extends React.PureComponent<Props> {
  componentDidMount() {
    if (this.props.orders.length === 0) {
      this.props.loadOrdersData();
    }
  }

  render() {
    const { orders, loading } = this.props;
    return (
      <div>
        <PageTitle title='History' />
        {loading ? <LinearLoader /> : <EnhancedTable orders={orders} />}
      </div>
    );
  }
}

export default connect(mapStateToProps, dispathToProps)(withStyles(styles)(History));
