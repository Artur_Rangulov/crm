import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Panel from './components/Panel/Panel';
import { links } from './defaultData/data';
import { withStyles, WithStyles } from '@material-ui/styles';
import { Drawer, CircularProgress } from '@material-ui/core';
import Dashboard from './containers/Dashboard/Dashboard';
import EB from './components/ErrorBoundary/ErrorBoundary';
import { styles } from './AppStyle';
const History = React.lazy(() => import('./containers/History/History'));
const AddOrder = React.lazy(() => import('./containers/AddOrder/AddOrder'));
const Analytics = React.lazy(() => import('./containers/Analytics/Analytics'));
const Ranges = React.lazy(() => import('./containers/Ranges/Ranges'));
const NotFount = React.lazy(() => import('./containers/NotFount/NotFount'));

type Props = WithStyles<typeof styles>

const App: React.FC<Props> = ({classes}) => {

  return (
    <Router>
      <div className={classes.container}>
        <div className={classes.toolbar}></div>
        <nav className={classes.navigation}>
          <Drawer
            variant="permanent"
            classes={{paper: classes.drawerPaper}}
          >
            <Panel links={links} />
          </Drawer>
        </nav>
        <main className={classes.content}>
          <EB>
            <Suspense fallback={<CircularProgress />}>
              <Switch>
                <Route path="/analytics" component={Analytics} />
                <Route path="/history" component={History} />
                <Route path="/addorder" component={AddOrder} />
                <Route exact path="/ranges" component={Ranges} />
                <Route exact path="/" component={Dashboard} />
                <Route component={NotFount} />
              </Switch> 
            </Suspense>
          </EB>
        </main>
      </div>
    </Router>
  )
}


export default withStyles(styles)(App);
