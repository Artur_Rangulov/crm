import { createMuiTheme } from "@material-ui/core";
import { responsiveFontSizes } from "@material-ui/core/styles";

declare module '@material-ui/core/styles/createMuiTheme' {
  interface ThemeOptions {
    panel?: any;
    content?: any;
    card?: any;
  }
};

const colors = {
  primary: '#6d7bff',
  grey: '#939395',
  lightGrey: '#f6f6f8',
  darkGrey: '#8b9dff',
  white: '#fff',
  smokyWhite: '#fefeff',
  black: '#1e1f21',
}

const materialUiTheme = createMuiTheme({
  typography: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontSize: 13,
    fontWeightLight: 400,
    fontWeightRegular: 500,
    fontWeightMedium: 600,
  },
  palette: {
    primary: {
      main: colors.primary,
    },
  },
  panel: {
    background: colors.smokyWhite,
    color: colors.grey,
    activeItem: {
      color: colors.darkGrey,
      background: colors.white,
      'box-shadow': '0px 0px 6px 0px rgba(0,0,0,0.6)',
    }
  },
  content: {
    background: colors.lightGrey,
    title: {
      color: colors.black,
    },
  },
  card: {
    background: colors.white,
    borderRadius: 10,
    overflow: 'hidden',
    'box-shadow': '2px 2px 8px 0px rgba(0,0,0,0.3)',
  }

});

export default responsiveFontSizes(materialUiTheme)