import React from 'react';
import { ResponsiveBar } from '@nivo/bar';


const data = [
  {
    "country": "AD",
    "hot dog": 196,
    "hot dogColor": "hsl(266, 70%, 50%)",
    "burger": 134,
    "burgerColor": "hsl(53, 70%, 50%)",
    "sandwich": 193,
    "sandwichColor": "hsl(15, 70%, 50%)",
    "kebab": 197,
    "kebabColor": "hsl(251, 70%, 50%)",
    "fries": 136,
    "friesColor": "hsl(33, 70%, 50%)",
    "donut": 54,
    "donutColor": "hsl(153, 70%, 50%)"
  },
  {
    "country": "AE",
    "hot dog": 162,
    "hot dogColor": "hsl(88, 70%, 50%)",
    "burger": 15,
    "burgerColor": "hsl(156, 70%, 50%)",
    "sandwich": 145,
    "sandwichColor": "hsl(27, 70%, 50%)",
    "kebab": 159,
    "kebabColor": "hsl(20, 70%, 50%)",
    "fries": 106,
    "friesColor": "hsl(226, 70%, 50%)",
    "donut": 80,
    "donutColor": "hsl(260, 70%, 50%)"
  },
  {
    "country": "AF",
    "hot dog": 70,
    "hot dogColor": "hsl(317, 70%, 50%)",
    "burger": 200,
    "burgerColor": "hsl(321, 70%, 50%)",
    "sandwich": 66,
    "sandwichColor": "hsl(53, 70%, 50%)",
    "kebab": 22,
    "kebabColor": "hsl(307, 70%, 50%)",
    "fries": 100,
    "friesColor": "hsl(48, 70%, 50%)",
    "donut": 37,
    "donutColor": "hsl(39, 70%, 50%)"
  },
  {
    "country": "AG",
    "hot dog": 161,
    "hot dogColor": "hsl(81, 70%, 50%)",
    "burger": 161,
    "burgerColor": "hsl(43, 70%, 50%)",
    "sandwich": 66,
    "sandwichColor": "hsl(127, 70%, 50%)",
    "kebab": 31,
    "kebabColor": "hsl(74, 70%, 50%)",
    "fries": 120,
    "friesColor": "hsl(201, 70%, 50%)",
    "donut": 26,
    "donutColor": "hsl(198, 70%, 50%)"
  },
  {
    "country": "AI",
    "hot dog": 93,
    "hot dogColor": "hsl(329, 70%, 50%)",
    "burger": 173,
    "burgerColor": "hsl(255, 70%, 50%)",
    "sandwich": 106,
    "sandwichColor": "hsl(269, 70%, 50%)",
    "kebab": 34,
    "kebabColor": "hsl(126, 70%, 50%)",
    "fries": 6,
    "friesColor": "hsl(185, 70%, 50%)",
    "donut": 30,
    "donutColor": "hsl(57, 70%, 50%)"
  },

]

const BarChart: React.FunctionComponent = () => (
  <ResponsiveBar
    data={data}
    keys={['sandwich', 'kebab', 'fries', 'donut']}
    indexBy="country"
    margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
    padding={0.3}
    colors={{ scheme: 'nivo' }}
    defs={[
      {
        id: 'dots',
        type: 'patternDots',
        background: 'inherit',
        color: '#38bcb2',
        size: 4,
        padding: 1,
        stagger: true
      },
      {
        id: 'lines',
        type: 'patternLines',
        background: 'inherit',
        color: '#eed312',
        rotation: -45,
        lineWidth: 6,
        spacing: 10
      }
    ]}
    fill={[
      {
        match: {
          id: 'fries'
        },
        id: 'dots'
      },
      {
        match: {
          id: 'sandwich'
        },
        id: 'lines'
      }
    ]}
    axisTop={null}
    axisRight={null}
    axisBottom={{
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: 'country',
      legendPosition: 'middle',
      legendOffset: 32
    }}
    axisLeft={{
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: 'food',
      legendPosition: 'middle',
      legendOffset: -40
    }}
    labelSkipWidth={12}
    labelSkipHeight={12}
    labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
    legends={[
      {
        dataFrom: 'keys',
        anchor: 'bottom-right',
        direction: 'column',
        justify: false,
        translateX: 120,
        translateY: 0,
        itemsSpacing: 2,
        itemWidth: 100,
        itemHeight: 20,
        itemDirection: 'left-to-right',
        itemOpacity: 0.85,
        symbolSize: 20,
        effects: [
          {
            on: 'hover',
            style: {
              itemOpacity: 1
            }
          }
        ]
      }
    ]}
    animate={true}
    motionStiffness={90}
    motionDamping={15}
  />
)

export default BarChart