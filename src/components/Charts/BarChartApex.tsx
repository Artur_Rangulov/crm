import React from "react";
import Chart from "react-apexcharts";

interface Props {
  orders: any;
}

const BarChartApex: React.FunctionComponent<Props> = ({ orders }) => {
  return (
    <Chart
      width="100%"
      height="100%"
      type="line"
      options={{
        chart: {
          shadow: {
            enabled: true,
            color: '#000',
            top: 18,
            left: 7,
            blur: 10,
            opacity: 1
          },
          toolbar: {
            show: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'straight'
        },
        title: {
          text: 'Общая выручка по дням',
          align: 'left'
        },
        grid: {
          row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
          },
        },
        xaxis: {
          categories: orders.categories,
          title: {
            text: 'Дни'
          }
        },
        yaxis: {
          title: {
            text: 'Стоимость (руб.)'
          },
        },
      }}
      series={[
        {
          name: "series-1",
          data: orders.seriesData
        },
      ]
      }
    />
  );
}

export default BarChartApex;