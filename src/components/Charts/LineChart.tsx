import React from 'react';
import { ResponsiveLine } from '@nivo/line';


const data = [
  {
    "id": "japan",
    "color": "hsl(221, 70%, 50%)",
    "data": [
      {
        "x": "plane",
        "y": 168
      },
      {
        "x": "helicopter",
        "y": 84
      },
      {
        "x": "boat",
        "y": 25
      },
      {
        "x": "train",
        "y": 160
      },
      {
        "x": "subway",
        "y": 123
      },
      {
        "x": "bus",
        "y": 198
      },
      {
        "x": "car",
        "y": 169
      },
      {
        "x": "moto",
        "y": 59
      },
      {
        "x": "bicycle",
        "y": 280
      },
      {
        "x": "horse",
        "y": 114
      },
      {
        "x": "skateboard",
        "y": 177
      },
      {
        "x": "others",
        "y": 64
      }
    ]
  },
  {
    "id": "france",
    "color": "hsl(207, 70%, 50%)",
    "data": [
      {
        "x": "plane",
        "y": 109
      },
      {
        "x": "helicopter",
        "y": 181
      },
      {
        "x": "boat",
        "y": 31
      },
      {
        "x": "train",
        "y": 25
      },
      {
        "x": "subway",
        "y": 96
      },
      {
        "x": "bus",
        "y": 133
      },
      {
        "x": "car",
        "y": 143
      },
      {
        "x": "moto",
        "y": 293
      },
      {
        "x": "bicycle",
        "y": 209
      },
      {
        "x": "horse",
        "y": 258
      },
      {
        "x": "skateboard",
        "y": 124
      },
      {
        "x": "others",
        "y": 199
      }
    ]
  },
  {
    "id": "us",
    "color": "hsl(341, 70%, 50%)",
    "data": [
      {
        "x": "plane",
        "y": 176
      },
      {
        "x": "helicopter",
        "y": 1
      },
      {
        "x": "boat",
        "y": 244
      },
      {
        "x": "train",
        "y": 151
      },
      {
        "x": "subway",
        "y": 295
      },
      {
        "x": "bus",
        "y": 51
      },
      {
        "x": "car",
        "y": 185
      },
      {
        "x": "moto",
        "y": 133
      },
      {
        "x": "bicycle",
        "y": 78
      },
      {
        "x": "horse",
        "y": 98
      },
      {
        "x": "skateboard",
        "y": 69
      },
      {
        "x": "others",
        "y": 134
      }
    ]
  },
  {
    "id": "germany",
    "color": "hsl(129, 70%, 50%)",
    "data": [
      {
        "x": "plane",
        "y": 207
      },
      {
        "x": "helicopter",
        "y": 1
      },
      {
        "x": "boat",
        "y": 181
      },
      {
        "x": "train",
        "y": 124
      },
      {
        "x": "subway",
        "y": 59
      },
      {
        "x": "bus",
        "y": 71
      },
      {
        "x": "car",
        "y": 183
      },
      {
        "x": "moto",
        "y": 10
      },
      {
        "x": "bicycle",
        "y": 169
      },
      {
        "x": "horse",
        "y": 281
      },
      {
        "x": "skateboard",
        "y": 99
      },
      {
        "x": "others",
        "y": 150
      }
    ]
  },
  {
    "id": "norway",
    "color": "hsl(26, 70%, 50%)",
    "data": [
      {
        "x": "plane",
        "y": 27
      },
      {
        "x": "helicopter",
        "y": 222
      },
      {
        "x": "boat",
        "y": 120
      },
      {
        "x": "train",
        "y": 127
      },
      {
        "x": "subway",
        "y": 151
      },
      {
        "x": "bus",
        "y": 160
      },
      {
        "x": "car",
        "y": 34
      },
      {
        "x": "moto",
        "y": 131
      },
      {
        "x": "bicycle",
        "y": 55
      },
      {
        "x": "horse",
        "y": 87
      },
      {
        "x": "skateboard",
        "y": 273
      },
      {
        "x": "others",
        "y": 2
      }
    ]
  }
]

const LineChart: React.FunctionComponent = () => (
  <ResponsiveLine
    data={data}
    margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
    xScale={{ type: 'point' }}
    yScale={{ type: 'linear', stacked: true, min: 'auto', max: 'auto' }}
    axisTop={null}
    axisRight={null}
    axisBottom={{
      orient: 'bottom',
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: 'transportation',
      legendOffset: 36,
      legendPosition: 'middle'
    }}
    axisLeft={{
      orient: 'left',
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      legend: 'count',
      legendOffset: -40,
      legendPosition: 'middle'
    }}
    colors={{ scheme: 'nivo' }}
    pointSize={10}
    pointColor={{ theme: 'background' }}
    pointBorderWidth={2}
    pointBorderColor={{ from: 'serieColor' }}
    pointLabel="y"
    pointLabelYOffset={-12}
    useMesh={true}
    legends={[
      {
        anchor: 'bottom-right',
        direction: 'column',
        justify: false,
        translateX: 100,
        translateY: 0,
        itemsSpacing: 0,
        itemDirection: 'left-to-right',
        itemWidth: 80,
        itemHeight: 20,
        itemOpacity: 0.75,
        symbolSize: 12,
        symbolShape: 'circle',
        symbolBorderColor: 'rgba(0, 0, 0, .5)',
        effects: [
          {
            on: 'hover',
            style: {
              itemBackground: 'rgba(0, 0, 0, .03)',
              itemOpacity: 1
            }
          }
        ]
      }
    ]}
  />
)

export default LineChart