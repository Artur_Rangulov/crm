import React from 'react';
import { withStyles, WithStyles, createStyles } from '@material-ui/core';

const styles = (theme: any) => createStyles({
  component: {
    marginTop: 20,
    padding: 10,
    flexBasis: (props: any) => props.flexbasis || '24%',
  }
});

interface iProps {
  className?: any,
  flexbasis?: string,
  children?:any,
};

type Props = iProps & WithStyles<typeof styles>;

const Contol: React.FunctionComponent<Props> = (props) => {
  const { classes, children, className, ...other } = props;
  return (
    <div className={`${classes.component} ${className}`}  {...other}>
        {children}
    </div>
  );
}

export default withStyles(styles)(Contol);
