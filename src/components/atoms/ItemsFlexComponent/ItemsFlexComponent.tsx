import React from 'react';
import { withStyles, WithStyles, createStyles } from '@material-ui/core';

const styles = (theme: any) => createStyles({
  component: {
    marginTop: 20,
    padding: 10,
    flexBasis: (props: any) => props.flexbasis || '24%',
    minHeight: (props: any) => props.minheight || '100px',
    maxHeight: (props: any) => props.maxheight,
    minWidth: (props: any) => props.minwidth || '100px',
    maxWidth: (props: any) => props.maxwidth,
  }
});

interface iProps {
  className?: any,
  minheight?: string,
  maxheight?: string,
  minwidth?: string,
  maxwidth?: string,
  flexbasis?: string,
  children?:any,
};

type Props = iProps & WithStyles<typeof styles>;

const ItemsFlexComponent: React.FunctionComponent<Props> = (props) => {
  const { classes, children, className, ...other } = props;
  return (
    <div className={`${classes.component} ${className}`}  {...other}>
        {children}
    </div>
  );
}

export default withStyles(styles)(ItemsFlexComponent);
