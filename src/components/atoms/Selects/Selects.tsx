import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/styles';

interface ItemType{
  value: string | number,
  title: string | number,
}

interface Props{
  selectTitle: string;
  items: ItemType[];
  selectValue: any;
}

const useStyles = makeStyles(() => ({
  formControl: {
    marginTop: "10px",
    minWidth: "100%",
  },
}));

const Selects: React.FunctionComponent<Props> = ({ selectTitle, items, selectValue}) => {
  const classes = useStyles();
  const [selectedValue, setSelectedValue] = React.useState('');
  const [open, setOpen] = React.useState(false);

  function handleChange(event: any) {
    setSelectedValue(event.target.value);
    selectValue(event.target.value)
  }

  function handleClose() {
    setOpen(false);
  }

  function handleOpen() {
    setOpen(true);
  }

  return (
    <FormControl className={classes.formControl}>
      <InputLabel htmlFor="demo-controlled-open-select">{selectTitle}</InputLabel>
      <Select
        open={open}
        onClose={handleClose}
        onOpen={handleOpen}
        value={selectedValue}
        onChange={handleChange}
        inputProps={{
          name: 'age',
          id: 'demo-controlled-open-select',
        }}
      >
        {
          items && items.map( (item: ItemType) => <MenuItem key={item.value} value={item.value}>{item.title}</MenuItem>)
        }
      </Select>
    </FormControl>
  )
}

export default Selects;
