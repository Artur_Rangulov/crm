import React from 'react';
import { withStyles, WithStyles, createStyles, Typography, Button, Badge } from '@material-ui/core';

export const styles = (theme: any) => createStyles({
  pageHeader: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    color: theme.content.title.color
  }
});

interface iProps {
  title: string;
  buttonTitle?: string;
  badgeCount?: number | string;
  showButton?: boolean;
  showBadge?: boolean;
  children?: any;
  onClick?: any;
};

type Props = iProps & WithStyles<typeof styles>;

const PageTitle: React.FunctionComponent<Props> = ({
  title,
  showButton,
  buttonTitle,
  badgeCount,
  showBadge,
  classes,
  onClick
}) => {
  const button = showButton ? <Button color='primary' variant="contained" onClick={onClick}>{buttonTitle}</Button> : null;
  const badge = showBadge ? <Badge badgeContent={badgeCount} color="secondary"> {button}</Badge> : null;
  return (
    <div className={classes.pageHeader}>
      <Typography variant='h4' component='h1' className={classes.title}>{title}</Typography>
      {showBadge ? badge : showButton ? button : null}
    </div>
  )
}

export default withStyles(styles)(PageTitle);
