import React, { } from 'react';
import { Fab, WithStyles } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { withStyles, createStyles } from '@material-ui/styles';

interface iProps{
  title: string,
  onClick: any,
}

const styles = createStyles({
  productItem: {
    listStyle: "none",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: '10px'
  }
})
type Props = iProps & WithStyles<typeof styles>

const ProductItem: React.FunctionComponent<Props> = (props) => {
  const { title, classes, onClick} = props;
  return (
    <li className={classes.productItem}>
      <div>
        {title}
      </div>
      <div>
        <Fab color="primary" aria-label="add" size="small" onClick={onClick}>
          <AddIcon />
        </Fab>
      </div>
    </li>
  )
}

export default withStyles(styles)(ProductItem);
