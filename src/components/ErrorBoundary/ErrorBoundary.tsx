import * as React from "react";
type Props = {
  errorMessage?: React.ReactNode;
}
type State = {
  hasError: boolean
}
class EB extends React.Component<Props, State> {
  state = { hasError: false };

  static getDerivedStateFromError(error: any) {
    return { hasError: true };
  }
  componentDidCatch(error: any, info: any) {
    console.error(error, info);
  }
  render() {
    if (this.state.hasError) {
      const { errorMessage } = this.props;
      return errorMessage || 'Something went wrong, please contact the developer';
    }
    return this.props.children;
  }
}
export default EB;