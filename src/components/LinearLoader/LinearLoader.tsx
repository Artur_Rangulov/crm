import React from 'react';
import { LinearProgress } from '@material-ui/core';

interface Props{
  children?: any
}

const LinearLoader: React.FunctionComponent<Props> = (props) => (
  <>
    <div className='content'>
      <LinearProgress />
      {props.children}
    </div>
  </>
)

export default LinearLoader
