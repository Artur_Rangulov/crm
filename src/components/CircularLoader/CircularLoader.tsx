import React from 'react';
import { CircularProgress } from '@material-ui/core';

interface Props{
  children?: any
}

const CircularLoader : React.FunctionComponent<Props> = (props) => (
  <>
    <div className='content'>
      <CircularProgress />
      {props.children}
    </div>
  </>
)

export default CircularLoader
