import React from 'react'
import {
  createStyles,
  withStyles,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography
} from '@material-ui/core';
import { WithStyles } from '@material-ui/styles';
import ItemsFlexComponent from '../atoms/ItemsFlexComponent/ItemsFlexComponent';

export const styles = createStyles({
  media: {
    height: 140,
  },
});

interface iProps {
  title: string;
  img: string;
  onClick: any;
};

type Props = iProps & WithStyles<typeof styles>;

const RangeCard: React.FunctionComponent<Props> = ({ classes, img, title, onClick, ...other}) => {
  return (
    <ItemsFlexComponent flexbasis='25%'>
      <Card >
        <CardActionArea onClick={onClick}>
          <CardMedia
            className={classes.media}
            image={img}
            title="title"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2" align="center">
              {title}
          </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </ItemsFlexComponent>
  )
}

export default withStyles(styles)(RangeCard);
