import React from 'react';
import { withStyles, WithStyles, createStyles } from '@material-ui/core';
import RangeCard from './RangeCard';
import { RangeType } from '../../redux/ranges/store';

export const styles = createStyles({
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: 30,
  },
});

interface iProps {
  ranges: RangeType[],
  openAddOrderDialog: any,
};

type Props = iProps & WithStyles<typeof styles>;

const RangeCards: React.FunctionComponent<Props> = ({ classes, ranges, openAddOrderDialog }) => {
  return (
    <div className={classes.wrapper}>
      {
        ranges.map((range: RangeType) => (
          <RangeCard key={range.objectId} title={range.title} img={range.img} onClick={() => openAddOrderDialog(range.title)}/>
        ))
      }
    </div>
  );
}

export default withStyles(styles)(RangeCards);
