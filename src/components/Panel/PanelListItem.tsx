import React, { ReactElement } from 'react';
import {
  ListItemIcon,
  ListItem,
  ListItemText,
  WithStyles,
  createStyles,
  withStyles
} from '@material-ui/core';
// import InboxIcon from '@material-ui/icons/MoveToInbox';
import { NavLink } from 'react-router-dom';

interface IProps{
  label: string,
  link: string,
  icon: ReactElement,
}

const styles = (theme: any) => createStyles({
  activeLink: {
    '& > div': {
      ...theme.panel.activeItem
    },
    '& panelIcon': {
      color: theme.panel.activeItem.color,
    }
  },
  panelIcon: {
    color: theme.panel.color,
  },
  panelItemTitle: {
    color: theme.panel.color,
  }
})

type Props = IProps & WithStyles<typeof styles>

const PanelListItem: React.FC<Props> = ({ label, link, icon, classes}) => (
  <>
    <NavLink exact to={link} activeClassName={classes.activeLink}>
      <ListItem button >
        <ListItemIcon className={classes.panelIcon}>{icon}</ListItemIcon>
        <ListItemText className={classes.panelItemTitle} primary={label} />
      </ListItem>
    </NavLink>
  </>
)

export default withStyles(styles)(PanelListItem);
