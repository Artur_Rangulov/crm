import React from 'react';
import {
  Toolbar,
  Typography,
  List,
} from '@material-ui/core';
import PanelListItem from './PanelListItem';
import { Link } from '../../models/models';

interface iProps {
  links: Link[];
}

type Props = iProps

const Panel: React.FC<Props> = ({ links }) => (
  <>
    <Toolbar>
      <Typography variant="h5" align="center">
        CRM
      </Typography>
    </Toolbar>
    <List>
      {links.map(link => (
        <PanelListItem 
          label={link.label}
          link={link.link}
          key={link.id}
          icon={link.icon}
          />
      ))}
    </List>
  </>
)


export default (Panel);
