import React from 'react';
import { withStyles, WithStyles, createStyles, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

export const styles = createStyles({
  addOrder: {
    position: 'fixed',
    bottom: 20,
    right: 20,
    zIndex: 9999,
  }
});

interface iProps {
  onClick: React.MouseEventHandler,
};

type Props = iProps & WithStyles<typeof styles>;

const SimpleAddOrder: React.FunctionComponent<Props> = ({ classes, onClick }) => {
  return (
    <Fab color="primary" aria-label="add" className={classes.addOrder} onClick={onClick}>
      <AddIcon />
    </Fab>
  );
}

export default withStyles(styles)(SimpleAddOrder);
