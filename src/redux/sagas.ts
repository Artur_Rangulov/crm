import { all } from "redux-saga/effects";
import { rangeWatcherSagas } from "./ranges/sagas";
import { orderWatcherSagas } from "./addorder/sagas";
import { ordersWatcherSagas } from "./history/sagas";

export function* watchAll() {
  yield all([
    ...rangeWatcherSagas,
    ...orderWatcherSagas,
    ...ordersWatcherSagas,
    // ...dashboardWatcherSagas,
    // ...analyticsWatcherSagas,
    // ...historyWatcherSagas,
    // ...addorderWatcherSagas,
    // ...rangeWatcherSagas,
  ]);
}

export enum RequestType {
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
  DELETE = "DELETE",
}

export const getRequestOptions = (method: RequestType, body?: BodyInit): RequestInit => {
  return {
    headers: {
      accept: "application/json", "Content-Type": "application/json",
    },
    mode: "cors",
    cache: "no-cache",
    method,
    body,
  };
};

export const responseBodyErrorHandler = (
  response: Response,
  bodyHandler: ((response: Response, body: any) => void) | null,
  catchHandler?: (response: Response, error: any) => void,
) => {
  const isNotOK = !response.ok;
  return response
    .json()
    .then((body) => {
      if (bodyHandler && isNotOK) {
        bodyHandler(response, body);
      } else {
        return body;
      }
    })
    .catch((error) => {
      if (catchHandler && isNotOK) {
        catchHandler(response, error);
      }
    });
};

export const bodyErrorHandler = (response: Response) => {
  return responseBodyErrorHandler(response, null, (response: Response, error: any) => {
    throwStandartResponseError(response);
  });
};

export const throwStandartResponseError = (response: Response) => {
  throw new Error(`Error ${response.status}: ${response.statusText}`);
};

export const throwConductor = (error: ErrorEvent) => {
  throw error;
};