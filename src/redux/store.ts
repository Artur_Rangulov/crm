import { createStore, applyMiddleware, Store } from "redux";
import createSagaMiddleware from "redux-saga";
import { watchAll } from "./sagas";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducer";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import { IRangeStore } from "./ranges/store";
import { IOrderStore } from "./addorder/store";

export interface IStore {
  range: IRangeStore;
  orders: IOrderStore;
}

export const history = createBrowserHistory()

const sagaMiddleware = createSagaMiddleware();

const store: Store<IStore> = createStore(
  rootReducer(history),
  composeWithDevTools(applyMiddleware(sagaMiddleware, routerMiddleware(history))),
);

sagaMiddleware.run(watchAll);

export default store;