import { ActionTypes } from "../actionTypes";
import {
  ILoadRangeData,
  ILoadRangeDataSuccess,
  ILoadRangeDataFailed,
  IAddNewRange,
  IAddNewRangeSuccess,
  IAddNewRangeFailed,
  IDeleteRange,
  IDeleteRangeSuccess,
  IDeleteRangeFailed,
  ILoadGoodsData,
  ILoadGoodsDataFailed,
  ILoadGoodsDataSuccess,
  ICleanGoodsData,
  IAddNewGood,
  IAddNewGoodSuccess,
  IAddNewGoodFailed,
  ISaveEditRange,
  ISaveEditRangeFailed,
  ISaveEditRangeSuccess,
  ISaveEditGood,
  ISaveEditGoodFailed,
  ISaveEditGoodSuccess,
  IDeleteGood,
  IDeleteGoodSuccess,
  IDeleteGoodFailed } from "./actionTypes";
import { RangeType, GoodsType } from "./store";

export const loadRangeData: () => ILoadRangeData = () => ({
  type: ActionTypes.RANGES_FETCH_LOAD
})

export const loadRangeDataSuccess: (ranges: RangeType[], goods: GoodsType[]) => ILoadRangeDataSuccess = (ranges, goods) => ({
  type: ActionTypes.RANGES_FETCH_LOAD_SUCCEEDED,
  ranges,
  goods,
})

export const loadRangeDataFailed: () => ILoadRangeDataFailed = () => ({
  type: ActionTypes.RANGES_FETCH_LOAD_FAILED,
})

export const loadGoodsData: (rangeTitle: string) => ILoadGoodsData = (rangeTitle) => ({
  type: ActionTypes.RANGES_FETCH_LOAD_GOODS,
  rangeTitle,
})

export const loadGoodsDataSuccess: (goods: GoodsType[]) => ILoadGoodsDataSuccess = (goods) => ({
  type: ActionTypes.RANGES_FETCH_LOAD_GOODS_SUCCEEDED,
  goods,
})

export const loadGoodsDataFailed: () => ILoadGoodsDataFailed = () => ({
  type: ActionTypes.RANGES_FETCH_LOAD_GOODS_FAILED,
})

export const cleanGoodsData: () => ICleanGoodsData = () => ({
  type: ActionTypes.RANGES_CLEAN_LOAD_GOODS,
})

export const addNewRange: (range: RangeType) => IAddNewRange = (range) => ({
  type: ActionTypes.RANGES_ADD_NEW,
  range,
})

export const addNewRangeSuccess: (range: RangeType) => IAddNewRangeSuccess = (range) => ({
  type: ActionTypes.RANGES_ADD_NEW_SUCCEEDED,
  range,
})

export const addNewRangeFailed: () => IAddNewRangeFailed = () => ({
  type: ActionTypes.RANGES_ADD_NEW_FAILED,
})

export const saveEditRange: (range: RangeType, callback: (success: boolean, message: string) => void) => ISaveEditRange = (range, callback) => ({
  type: ActionTypes.RANGES_SAVE_EDIT_RANGE,
  callback,
  range,
})

export const saveEditRangeSuccess: (range: RangeType) => ISaveEditRangeSuccess = (range) => ({
  type: ActionTypes.RANGES_SAVE_EDIT_RANGE_SUCCEEDED,
  range,
})

export const saveEditRangeFailed: () => ISaveEditRangeFailed = () => ({
  type: ActionTypes.RANGES_SAVE_EDIT_RANGE_FAILED,
})

export const saveEditGood: (good: GoodsType, callback: (success: boolean, message: string) => void) => ISaveEditGood = (good, callback) => ({
  type: ActionTypes.RANGES_SAVE_EDIT_GOOD,
  callback,
  good,
})

export const saveEditGoodSuccess: (good: GoodsType) => ISaveEditGoodSuccess = (good) => ({
  type: ActionTypes.RANGES_SAVE_EDIT_GOOD_SUCCEEDED,
  good,
})

export const saveEditGoodFailed: () => ISaveEditGoodFailed = () => ({
  type: ActionTypes.RANGES_SAVE_EDIT_GOOD_FAILED,
})

export const addNewGood: (good: GoodsType) => IAddNewGood = (good) => ({
  type: ActionTypes.RANGES_ADD_NEW_GOOD,
  good,
})

export const addNewGoodSuccess: (good: GoodsType) => IAddNewGoodSuccess = (good) => ({
  type: ActionTypes.RANGES_ADD_NEW_GOOD_SUCCEEDED,
  good,
})

export const addNewGoodFailed: () => IAddNewGoodFailed = () => ({
  type: ActionTypes.RANGES_ADD_NEW_GOOD_FAILED,
})

export const deleteGood: (objectId: string) => IDeleteGood = (objectId) => ({
  type: ActionTypes.RANGES_DELETE_GOOD,
  objectId,
})

export const deleteGoodSuccess: (objectId: string) => IDeleteGoodSuccess = (objectId) => ({
  type: ActionTypes.RANGES_DELETE_GOOD_SUCCEEDED,
  objectId,
})

export const deleteGoodFailed: () => IDeleteGoodFailed = () => ({
  type: ActionTypes.RANGES_DELETE_GOOD_FAILED,
})

export const deleteRange: (range: RangeType) => IDeleteRange = (range) => ({
  type: ActionTypes.RANGES_DELETE_RANGE,
  range,
})

export const deleteRangeSuccess: (range: RangeType) => IDeleteRangeSuccess = (range) => ({
  type: ActionTypes.RANGES_DELETE_RANGE_SUCCEEDED,
  range,
})

export const deleteRangeFailed: () => IDeleteRangeFailed = () => ({
  type: ActionTypes.RANGES_DELETE_RANGE_FAILED,
})