import { ActionTypes } from "../actionTypes";
import { RangeType, GoodsType } from "./store";

export type RangeActions =
  | ILoadRangeData
  | ILoadRangeDataSuccess
  | ILoadRangeDataFailed
  | IAddNewRange
  | IAddNewRangeSuccess
  | IAddNewRangeFailed
  | IDeleteRange
  | IDeleteRangeSuccess
  | IDeleteRangeFailed
  | ILoadGoodsData
  | ILoadGoodsDataSuccess
  | ILoadGoodsDataFailed
  | ICleanGoodsData
  | IAddNewGood
  | IAddNewGoodSuccess
  | IAddNewGoodFailed
  | IDeleteGood
  | IDeleteGoodSuccess
  | IDeleteGoodFailed
  | ISaveEditRange
  | ISaveEditRangeSuccess
  | ISaveEditRangeFailed
  | ISaveEditGood
  | ISaveEditGoodSuccess
  | ISaveEditGoodFailed

export type IRangesAction = {};

export interface ILoadRangeData extends IRangesAction {
  type: ActionTypes.RANGES_FETCH_LOAD
}

export interface ILoadRangeDataSuccess extends IRangesAction {
  type: ActionTypes.RANGES_FETCH_LOAD_SUCCEEDED,
  ranges: RangeType[],
  goods: GoodsType[],
}

export interface ILoadRangeDataFailed extends IRangesAction {
  type: ActionTypes.RANGES_FETCH_LOAD_FAILED,
}

export interface ILoadGoodsData extends IRangesAction {
  type: ActionTypes.RANGES_FETCH_LOAD_GOODS,
  rangeTitle: string,
}

export interface ILoadGoodsDataSuccess extends IRangesAction {
  type: ActionTypes.RANGES_FETCH_LOAD_GOODS_SUCCEEDED,
  goods: GoodsType[],
}

export interface ILoadGoodsDataFailed extends IRangesAction {
  type: ActionTypes.RANGES_FETCH_LOAD_GOODS_FAILED,
}

export interface ICleanGoodsData extends IRangesAction {
  type: ActionTypes.RANGES_CLEAN_LOAD_GOODS,
}

export interface IAddNewRange extends IRangesAction {
  type: ActionTypes.RANGES_ADD_NEW,
  range: RangeType,
}

export interface IAddNewRangeSuccess extends IRangesAction {
  type: ActionTypes.RANGES_ADD_NEW_SUCCEEDED,
  range: RangeType,
}

export interface IAddNewRangeFailed extends IRangesAction {
  type: ActionTypes.RANGES_ADD_NEW_FAILED,
}

export interface ISaveEditRange extends IRangesAction {
  type: ActionTypes.RANGES_SAVE_EDIT_RANGE,
  range: RangeType,
  callback: (success: boolean, message: string) => void,
}

export interface ISaveEditRangeSuccess extends IRangesAction {
  type: ActionTypes.RANGES_SAVE_EDIT_RANGE_SUCCEEDED,
  range: RangeType,
}

export interface ISaveEditRangeFailed extends IRangesAction {
  type: ActionTypes.RANGES_SAVE_EDIT_RANGE_FAILED,
}

export interface ISaveEditGood extends IRangesAction {
  type: ActionTypes.RANGES_SAVE_EDIT_GOOD,
  good: GoodsType,
  callback: (success: boolean, message: string) => void,
}

export interface ISaveEditGoodSuccess extends IRangesAction {
  type: ActionTypes.RANGES_SAVE_EDIT_GOOD_SUCCEEDED,
  good: GoodsType,
}

export interface ISaveEditGoodFailed extends IRangesAction {
  type: ActionTypes.RANGES_SAVE_EDIT_GOOD_FAILED,
}

export interface IAddNewGood extends IRangesAction {
  type: ActionTypes.RANGES_ADD_NEW_GOOD,
  good: GoodsType,
}

export interface IAddNewGoodSuccess extends IRangesAction {
  type: ActionTypes.RANGES_ADD_NEW_GOOD_SUCCEEDED,
  good: GoodsType,
}

export interface IAddNewGoodFailed extends IRangesAction {
  type: ActionTypes.RANGES_ADD_NEW_GOOD_FAILED,
}

export interface IDeleteGood extends IRangesAction {
  type: ActionTypes.RANGES_DELETE_GOOD,
  objectId: string,
}

export interface IDeleteGoodSuccess extends IRangesAction {
  type: ActionTypes.RANGES_DELETE_GOOD_SUCCEEDED,
  objectId: string,
}

export interface IDeleteGoodFailed extends IRangesAction {
  type: ActionTypes.RANGES_DELETE_GOOD_FAILED,
}

export interface IDeleteRange extends IRangesAction {
  type: ActionTypes.RANGES_DELETE_RANGE,
  range: RangeType,
}

export interface IDeleteRangeSuccess extends IRangesAction {
  type: ActionTypes.RANGES_DELETE_RANGE_SUCCEEDED,
  range: RangeType,
}

export interface IDeleteRangeFailed extends IRangesAction {
  type: ActionTypes.RANGES_DELETE_RANGE_FAILED,
}