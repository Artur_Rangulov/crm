import produce from "immer"
import { ActionTypes } from "../actionTypes";
import { RangeActions } from "./actionTypes";
import { IRangeStore } from "./store";

const initialState: IRangeStore = {
  ranges: [],
  goods: [],
  loading: false,
  error: false,
};


export const rangeReducer = (state: IRangeStore = initialState, action: RangeActions) => {
  switch (action.type) {
    case ActionTypes.RANGES_FETCH_LOAD:
      return produce(state, draft => {
        draft.loading = true;
      });
    case ActionTypes.RANGES_FETCH_LOAD_SUCCEEDED:
      return produce(state, draft => {
        draft.ranges = action.ranges;
        draft.goods = action.goods;
        draft.loading = false;
        draft.error = false;
      });
    case ActionTypes.RANGES_FETCH_LOAD_FAILED:
      return produce(state, draft => {
        draft.loading = false;
        draft.error = true;
      });
    case ActionTypes.RANGES_ADD_NEW:
      return produce(state, draft => {
      });
    case ActionTypes.RANGES_ADD_NEW_SUCCEEDED:
      return produce(state, draft => {
        draft.ranges = [...draft.ranges, action.range];
        draft.error = false;
      });
    case ActionTypes.RANGES_ADD_NEW_FAILED:
      return produce(state, draft => {
        draft.loading = false;
        draft.error = true;
      });
    case ActionTypes.RANGES_SAVE_EDIT_RANGE:
      return produce(state, draft => {
      });
    case ActionTypes.RANGES_SAVE_EDIT_RANGE_SUCCEEDED:
      return produce(state, draft => {
        let index = state.ranges.findIndex(range => range.objectId === action.range.objectId);
        if (index !== -1) {draft.ranges[index] = action.range};
        draft.error = false;
      });
    case ActionTypes.RANGES_SAVE_EDIT_RANGE_FAILED:
      return produce(state, draft => {
        draft.error = true;
      });
    case ActionTypes.RANGES_ADD_NEW_GOOD:
      return produce(state, draft => {
      });
    case ActionTypes.RANGES_ADD_NEW_GOOD_SUCCEEDED:
      return produce(state, draft => {
        draft.goods = [...draft.goods, action.good];
        draft.error = false;
      });
    case ActionTypes.RANGES_ADD_NEW_GOOD_FAILED:
      return produce(state, draft => {
        draft.error = true;
      });
    case ActionTypes.RANGES_SAVE_EDIT_GOOD:
      return produce(state, draft => {
      });
    case ActionTypes.RANGES_SAVE_EDIT_GOOD_SUCCEEDED:
      return produce(state, draft => {
        let index = state.goods.findIndex(good => good.objectId === action.good.objectId);
        if (index !== -1) { draft.goods[index] = action.good };
        draft.error = false;
      });
    case ActionTypes.RANGES_SAVE_EDIT_GOOD_FAILED:
      return produce(state, draft => {
        draft.error = true;
      });
    case ActionTypes.RANGES_DELETE_GOOD:
      return produce(state, draft => {
      });
    case ActionTypes.RANGES_DELETE_GOOD_SUCCEEDED:
      return produce(state, draft => {
        draft.goods = state.goods.filter((good) =>
          good.objectId !== action.objectId
        );
        draft.error = false;
      });
    case ActionTypes.RANGES_DELETE_GOOD_FAILED:
      return produce(state, draft => {
        draft.error = true;
      });
    case ActionTypes.RANGES_DELETE_RANGE:
      return produce(state, draft => {
      });
    case ActionTypes.RANGES_DELETE_RANGE_SUCCEEDED:
      return produce(state, draft => {
        draft.ranges = state.ranges.filter((range) =>
          range.objectId !== action.range.objectId
        );
        draft.goods = state.goods.filter((good) =>
          good.range !== action.range.title
        );
        draft.error = false;
      });
    case ActionTypes.RANGES_DELETE_RANGE_FAILED:
      return produce(state, draft => {
        draft.error = true;
      });
    default:
      return state
  }
};