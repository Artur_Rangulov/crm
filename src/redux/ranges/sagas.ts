import { call, put, takeLatest } from 'redux-saga/effects'
import { ActionTypes } from "../actionTypes";
import {
  getRequestOptions,
  bodyErrorHandler,
  throwConductor,
  RequestType
} from '../sagas';
import {
  loadRangeDataFailed,
  loadRangeDataSuccess,
  addNewRangeSuccess,
  addNewRangeFailed,
  deleteRangeSuccess,
  deleteRangeFailed, 
  addNewGoodSuccess,
  addNewGoodFailed,
  saveEditRangeSuccess,
  saveEditRangeFailed,
  deleteGoodSuccess,
  deleteGoodFailed,
  saveEditGoodSuccess,
  saveEditGoodFailed} from './actions';
import { BASE_URL } from '../constants';
import { IAddNewRange, IAddNewGood, ISaveEditRange, IDeleteGood, IDeleteRange, ISaveEditGood } from './actionTypes';
import { RangeType, GoodsType } from './store';

export const rangeWatcherSagas = [
  takeLatest(ActionTypes.RANGES_FETCH_LOAD, loadRanges),
  takeLatest(ActionTypes.RANGES_ADD_NEW, addNewRangeFetch),
  takeLatest(ActionTypes.RANGES_SAVE_EDIT_RANGE, saveEditRangeFetch),
  takeLatest(ActionTypes.RANGES_DELETE_GOOD, deleteGoodFetch),
  takeLatest(ActionTypes.RANGES_ADD_NEW_GOOD, addNewGoodFetch),
  takeLatest(ActionTypes.RANGES_SAVE_EDIT_GOOD, saveEditGoodFetch),
  takeLatest(ActionTypes.RANGES_DELETE_RANGE, deleteRangeFetch),
]

// RANGE REQUESTS TO SERVER

const getRangesFromServer = () => {
  return fetch(`${BASE_URL}/ranges?pageSize=100`, getRequestOptions(RequestType.GET))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

const addNewRangeToServer = (range: RangeType) => {
  return fetch(`${BASE_URL}/ranges`, getRequestOptions(RequestType.POST, JSON.stringify(range)))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

const saveEditRangeToServer = (range: RangeType) => {
  return fetch(`${BASE_URL}/ranges`, getRequestOptions(RequestType.PUT, JSON.stringify(range)))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

const deleteRangeFromServer = (range: RangeType) => {
  return fetch(`${BASE_URL}/ranges/${range.objectId}`, getRequestOptions(RequestType.DELETE, JSON.stringify(range)))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

// GOODS REQUESTS TO SERVER

const getGoodsFromServer = () => {
  return fetch(`${BASE_URL}/goods?pageSize=100`, getRequestOptions(RequestType.GET))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

const addNewGoodToServer = (good: GoodsType) => {
  return fetch(`${BASE_URL}/goods`, getRequestOptions(RequestType.POST, JSON.stringify(good)))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

const saveEditGoodToServer = (good: GoodsType) => {
  return fetch(`${BASE_URL}/goods`, getRequestOptions(RequestType.PUT, JSON.stringify(good)))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

const deleteGoodFromServer = (objectId: string) => {
  return fetch(`${BASE_URL}/goods/${objectId}`, getRequestOptions(RequestType.DELETE, JSON.stringify(objectId)))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

const deleteGoodsFromServer = (rangeTitle: string) => {
  return fetch(`${BASE_URL}/bulk/goods?where=range%3D'${rangeTitle}'`, getRequestOptions(RequestType.DELETE, JSON.stringify(rangeTitle)))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

// RANGES ACTIONS

export function* loadRanges() {
  try {
    const ranges = yield call(getRangesFromServer);
    const goods = yield call(getGoodsFromServer);
    yield put(loadRangeDataSuccess(ranges, goods));
  } catch (e) {
    yield put(loadRangeDataFailed());
  }
}

export function* addNewRangeFetch(action: IAddNewRange) {
  try {
    const res = yield call(addNewRangeToServer, action.range);
    yield put(addNewRangeSuccess(res));
  } catch (e) {
    yield put(addNewRangeFailed());
  }
}

export function* saveEditRangeFetch(action: ISaveEditRange) {
  try {
    const res = yield call(saveEditRangeToServer, action.range);
    yield action.callback(true, "Изменения сохранены");
    yield put(saveEditRangeSuccess(res));
  } catch (e) {
    yield action.callback(false, "Произошла ошибка при сохранении, пожалуйста, повторите запрос");
    yield put(saveEditRangeFailed());
  }
}

export function* deleteRangeFetch(action: IDeleteRange) {
  try {
    yield call(deleteRangeFromServer, action.range);
    yield call(deleteGoodsFromServer, action.range.title);
    yield put(deleteRangeSuccess(action.range));
  } catch (e) {
    yield put(deleteRangeFailed());
  }
}

// GOODS ACTIONS

export function* addNewGoodFetch(action: IAddNewGood) {
  try {
    const res = yield call(addNewGoodToServer, action.good);
    yield put(addNewGoodSuccess(res));
  } catch (e) {
    yield put(addNewGoodFailed());
  }
}

export function* saveEditGoodFetch(action: ISaveEditGood) {
  try {
    const res = yield call(saveEditGoodToServer, action.good);
    yield action.callback(true, "Изменения сохранены");
    yield put(saveEditGoodSuccess(res));
  } catch (e) {
    yield action.callback(true, "Изменения сохранены");
    yield put(saveEditGoodFailed());
  }
}

export function* deleteGoodFetch(action: IDeleteGood) {
  try {
    yield call(deleteGoodFromServer, action.objectId);
    yield put(deleteGoodSuccess(action.objectId));
  } catch (e) {
    yield put(deleteGoodFailed());
  }
}