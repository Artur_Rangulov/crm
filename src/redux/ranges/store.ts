export interface RangeType{
  title: string;
  img: string;
  count?: number;
  objectId?: string;
}

export interface GoodsType {
  cost: number,
  img?: string,
  title: string,
  weight?: number,
  range: string,
  objectId?: string,
}

export interface IRangeStore {
  ranges: RangeType[],
  goods: GoodsType[],
  loading: boolean,
  error: boolean,
}
