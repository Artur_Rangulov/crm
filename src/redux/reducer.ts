import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router';
import { rangeReducer } from './ranges/reducer';
import { addNewOrderReducer } from './addorder/reducer';
import { ordersReducer } from './history/reducer';

export const rootReducer = (history: any) =>
  combineReducers({
    router: connectRouter(history),
    range: rangeReducer,
    order: addNewOrderReducer,
    orders: ordersReducer,
  });

export default rootReducer;
