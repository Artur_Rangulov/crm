import { call, put, takeLatest } from 'redux-saga/effects'
import { ActionTypes } from "../actionTypes";
import {
  getRequestOptions,
  bodyErrorHandler,
  throwConductor,
  RequestType
} from '../sagas';
import { loadOrdersDataSuccess, loadOrdersDataFailed } from './actions';
import { BASE_URL } from '../constants';

export const ordersWatcherSagas = [
  takeLatest(ActionTypes.ORDERS_FETCH_LOAD, loadOrders),
]


const getOrdersFromServer = () => {
  return fetch(`${BASE_URL}/history?pageSize=100&sortBy=created%20asc`, getRequestOptions(RequestType.GET))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

export function* loadOrders() {
  try {
    const orders = yield call(getOrdersFromServer);
    yield put(loadOrdersDataSuccess(orders));
  } catch (e) {
    yield put(loadOrdersDataFailed());
  }
}