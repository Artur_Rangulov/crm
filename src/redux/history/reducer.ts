import produce from "immer"
import { ActionTypes } from "../actionTypes";
import { OrdersActions } from "./actionTypes";
import { IOrderStore } from "../addorder/store";

const initialState: IOrderStore = {
  orders: [],
  loading: false,
  error: false,
};


export const ordersReducer = (state: IOrderStore = initialState, action: OrdersActions) => {
  switch (action.type) {
    case ActionTypes.ORDERS_FETCH_LOAD:
      return produce(state, draft => {
        draft.loading = true;
      });
    case ActionTypes.ORDERS_FETCH_LOAD_SUCCEEDED:
      return produce(state, draft => {
        draft.orders = action.orders;
        draft.loading = false;
        draft.error = false;
      });
    case ActionTypes.ORDERS_FETCH_LOAD_FAILED:
      return produce(state, draft => {
        draft.loading = false;
        draft.error = true;
      });
    default:
      return state
  }
};