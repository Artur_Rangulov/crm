import { ActionTypes } from "../actionTypes";
import {
  ILoadOrdersData,
  ILoadOrdersDataSuccess,
  ILoadOrdersDataFailed,
 } from "./actionTypes";
import { OrderType } from "../addorder/store";

export const loadOrdersData: () => ILoadOrdersData = () => ({
  type: ActionTypes.ORDERS_FETCH_LOAD
})

export const loadOrdersDataSuccess: (orders: OrderType[], ) => ILoadOrdersDataSuccess = (orders) => ({
  type: ActionTypes.ORDERS_FETCH_LOAD_SUCCEEDED,
  orders,
})

export const loadOrdersDataFailed: () => ILoadOrdersDataFailed = () => ({
  type: ActionTypes.ORDERS_FETCH_LOAD_FAILED,
})