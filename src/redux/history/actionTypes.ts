import { ActionTypes } from "../actionTypes";
import { OrderType } from "../addorder/store";

export type OrdersActions =
  | ILoadOrdersData
  | ILoadOrdersDataSuccess
  | ILoadOrdersDataFailed

export type IOrdersAction = {};

export interface ILoadOrdersData extends IOrdersAction {
  type: ActionTypes.ORDERS_FETCH_LOAD
}

export interface ILoadOrdersDataSuccess extends IOrdersAction {
  type: ActionTypes.ORDERS_FETCH_LOAD_SUCCEEDED,
  orders: OrderType[],
}

export interface ILoadOrdersDataFailed extends IOrdersAction {
  type: ActionTypes.ORDERS_FETCH_LOAD_FAILED,
}