import produce from "immer"
import { ActionTypes } from "../actionTypes";
import { OrderActions } from "./actionTypes";
import { IOrderStore } from "./store";

const initialState: IOrderStore = {
  orders: [],
  loading: false,
  error: false,
};


export const addNewOrderReducer = (state: IOrderStore = initialState, action: OrderActions) => {
  switch (action.type) {

    case ActionTypes.ORDERS_ADD_NEW:
      return produce(state, draft => {
      });
    case ActionTypes.ORDERS_ADD_NEW_SUCCEEDED:
      return produce(state, draft => {
        draft.error = false;
      });
    case ActionTypes.ORDERS_ADD_NEW_FAILED:
      return produce(state, draft => {
        draft.error = true;
      });
  
    default:
      return state
  }
};