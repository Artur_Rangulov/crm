import { ActionTypes } from "../actionTypes";
import {
  IAddNewOrder,
  IAddNewOrderSuccess,
  IAddNewOrderFailed,
 } from "./actionTypes";
import { OrderType } from "./store";

export const addNewOrder: (order: OrderType) => IAddNewOrder = (order) => ({
  type: ActionTypes.ORDERS_ADD_NEW,
  order,
})

export const addNewOrderSuccess: () => IAddNewOrderSuccess = () => ({
  type: ActionTypes.ORDERS_ADD_NEW_SUCCEEDED,
})

export const addNewOrderFailed: () => IAddNewOrderFailed = () => ({
  type: ActionTypes.ORDERS_ADD_NEW_FAILED,
})


