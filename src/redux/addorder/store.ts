export interface OrderType{
  amount: number;
  orders: string;
  totalCost: number;
  objectId?: string;
  created?: number;
  paymentMethod: string;
}

export interface IOrderStore{
  orders: OrderType[],
  loading: boolean,
  error: boolean;
}