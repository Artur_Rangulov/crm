import { ActionTypes } from "../actionTypes";
import { OrderType } from "./store";

export type OrderActions =
  | IAddNewOrder
  | IAddNewOrderSuccess
  | IAddNewOrderFailed

export type IOrdersAction = {};

export interface IAddNewOrder extends IOrdersAction {
  type: ActionTypes.ORDERS_ADD_NEW,
  order: OrderType,
}

export interface IAddNewOrderSuccess extends IOrdersAction {
  type: ActionTypes.ORDERS_ADD_NEW_SUCCEEDED,
}

export interface IAddNewOrderFailed extends IOrdersAction {
  type: ActionTypes.ORDERS_ADD_NEW_FAILED,
}

