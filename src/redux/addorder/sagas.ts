import { call, put, takeLatest } from 'redux-saga/effects'
import { ActionTypes } from "../actionTypes";
import {
  getRequestOptions,
  bodyErrorHandler,
  throwConductor,
  RequestType
} from '../sagas';
import { addNewOrderSuccess, addNewOrderFailed } from './actions';
import { BASE_URL } from '../constants';
import { OrderType } from './store';
import { IAddNewOrder } from './actionTypes';

export const orderWatcherSagas = [
  takeLatest(ActionTypes.ORDERS_ADD_NEW, addNewOrderFetch),
]

const addNewOrderToServer = (order: OrderType) => {
  return fetch(`${BASE_URL}/history`, getRequestOptions(RequestType.POST, JSON.stringify(order)))
    .then(bodyErrorHandler)
    .catch(throwConductor);
};

export function* addNewOrderFetch(action: IAddNewOrder) {
  try {
    yield call(addNewOrderToServer, action.order);
    yield put(addNewOrderSuccess());
  } catch (e) {
    yield put(addNewOrderFailed());
  }
}

