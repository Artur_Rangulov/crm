import { call, put, takeLatest } from 'redux-saga/effects'
import { ActionTypes } from "../actionTypes";
import {
  getRequestOptions,
  bodyErrorHandler,
  throwConductor,
  RequestType
} from '../sagas';
// import { addNewTaskSuccess, requestTasksSuccess, requestTasksError, addNewTaskError, deleteTaskSuccess, deleteTaskError } from './actions';

// export const todoWatcherSagas = [
//   takeLatest(ActionTypes.TASKS_FETCH_LOAD, loadTodos),
// ]

// const getTodosFromServer = () => {
//   return fetch(`${BASE_URL}/tasks`, getRequestOptions(RequestType.GET))
//     .then(bodyErrorHandler)
//     .catch(throwConductor);
// };

// export function* loadTodos() {
//   try {
//     const tasks = yield call(getTodosFromServer);
//     yield put(requestTasksSuccess(tasks));
//   } catch (e) {
//     yield put(requestTasksError());
//   }
// }