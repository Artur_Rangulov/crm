import React from 'react';
import { ConnectedRouter } from "connected-react-router";
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import store, { history } from './redux/store';
import './style.css'
import materialUiTheme from './theme/materialUiTheme';
import { MuiThemeProvider } from '@material-ui/core/styles';
// import { IntlProvider } from 'react-intl';
// import { addLocaleData } from "react-intl";
// import locale_en from 'react-intl/locale-data/en';
// import locale_ru from 'react-intl/locale-data/ru';
// import messages_ru from "./translations/ru.json";
// import messages_en from "./translations/en.json";

// const messages = {
//   'ru': messages_ru,
//   'en': messages_en
// };

// addLocaleData([...locale_en, ...locale_ru]);

// const language = 'ru'

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={materialUiTheme}>
      <ConnectedRouter history={history}>
        {/* <IntlProvider locale={language} messages={messages[language]}> */}
          <App />
        {/* </IntlProvider> */}
      </ConnectedRouter>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root'));
serviceWorker.unregister();
