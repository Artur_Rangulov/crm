import { ReactElement } from "react";

export interface Link {
  label: string,
  link: string,
  id: string,
  icon: ReactElement<any>,
}

export interface Genre {
  id: number;
  name: string;
}