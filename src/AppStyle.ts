import { createStyles } from "@material-ui/core";

const drawerWidth = 240;

export const styles = (theme: any) => createStyles({
  container: {
    display: 'grid',
    'grid-template-columns': `${drawerWidth}px 1fr`,
    'grid-template-rows': '1fr',
    'grid-template-areas': '"sidebar view"',
  },
  toolbar: {

  },
  content: {
    'grid-area': 'view',
    'min-height': '100vh',
    padding: '30px 50px',
    'box-sizing': 'border-box',
    background: theme.content.background,
  },
  drawerPaper: {
    width: `${drawerWidth}px`,
    background: theme.panel.background,
  },
  navigation: {
    'grid-area': 'sidebar',
  },
})
